/* Linhas */
% linha(ID_LINHA,[lista_estacoes]).

linha(1,[la_defense,esplanade_de_la_defense,pont_de_neuilly,les_sablons,neuilly_pte_maillot,charles_de_gaulle_etoilegeorge_v,franklin_d_roosevelt,champs_elysees_clemenceau,concorde,tuileries,palais_royal_musee_du_louvre,louvre_rivoli,chatelet,hotel_de_ville,saint_paul,bastille,gare_de_lyon,reuilly_diderot,nation,porte_de_vincennes,saint_mande,berault,chateau_de_vincennes]).
linha(2,[porte_dauphine,victor_hugo,charles_de_gaulle_etoile,ternes,courcelles,monceau,villiers,rome,place_de_clichy,blanche,pigalle,anvers,barbes_rochechouart,la_chapelle,stalingrad,jaures,colonel_fabien,belleville,couronnes,menilmontant,pere_lachaise,philippe_auguste,alexandre_dumas,avron,nation]).
linha(3,[pont_de_levallois_becon,anatole_france,louise_michel,porte_de_champerret,pereire,wagram,malesherbes,villiers,europe,saint_lazare,havre_caumartin,opera,quatre_septembre,bourse,sentier,reaumur_sebastopol,arts_et_metiers,temple,republique,parmentier,rue_saint_maur,pere_lachaise,gambetta,porte_de_bagnolet,gallieni]).
linha(4,[porte_de_clignancourt,simplon,marcadet_poissonniers,chateau_rouge,barbes_rochechouart,gare_du_nord,gare_de_lest,chateau_deau,strasbourg_saint_denis,reaumur_sebastopol,etienne_marcel,les_halles,chatelet,cite,saint_michel,odeon,saint_germain_des_pres,saint_sulpice,saint_placide,montparnasse_bienvenue,vavin,raspail,denfert_rochereau,mouton_duvernet,alesia,porte_dorleans,mairie_de_montrouge]).
linha(5,[bobigny_pablo_picasso,bobigny_pantin_raymon_queneau,eglise_de_pantin,hoche,porte_de_pantin,ourcq,laumiere,jaures,stalingrad,gare_du_nord,gare_de_l_est,jacques_bonsergent,republique,oberkampf,richard_lenoir,breguet_sabin,bastille,quai_de_la_rapee,gare_dausterlitz,saint_marcel,campo_formio,place_ditalie]).

linha(6,[charles_de_gaulle_etoile,kleber,boissiere,trocadero,passy,bir_hakeim,dupleix,la_motte_picquet_grenelle,cambronne,sevres_lecourbe,pasteur,montparnasse_bienvenue,edgar_quinet,raspail,saint_jacques,glaciere,corvisart,place_ditalie,nationale,chevaleret,quai_de_la_gare,bercy,dugommier,daumesnil,bel_air,picpus,nation]).
/* Linha 7 < sentido 1 > MARIE_D'IVRY */
linha(7,[la_courneuve_8_Mai_1945,fort_d_aubervilliers,aubervilliers_pantin_quatre_chemins,porte_de_la_villette,corentin_cariou,crimee,riquet,stalingrad,louis_blanc,chateau_landon,gare_de_l_est,poissonniere,cadet,le_peletier,chaussee_d_antin_la_fayette,opera,pyramides,palais_royal_musee_du_louvre,pont_neuf,chatelet,pont_marie,sully_morland,jussieu,place_monge,censier_daubenton,les_gobelins,place_d_italie,tolbiac,maison_blanche,porte_d_italie,porte_de_choisy,porte_d_ivry,pierre_et_marie_curie,mairie_d_ivry]).
/* linha 7 < sentido 2 > Villejuif_Louis Aragon */
linha(7,[la_courneuve_8_Mai_1945,fort_d_aubervilliers,aubervilliers_pantin_quatre_chemins,porte_de_la_villette,corentin_cariou,crimee,riquet,stalingrad,louis_blanc,chateau_landon,gare_de_l_est,poissonniere,cadet,le_peletier,chaussee_d_antin_la_fayette,opera,pyramides,palais_royal_musee_du_louvre,pont_neuf,chatelet,pont_marie,sully_morland,jussieu,place_monge,censier_daubenton,les_gobelins,place_d_italie,tolbiac,maison_blanche,le_kremlin_bicetre,villejuif_leo_lagrange,villejuif_paul_vaillant_couturier,villejuif_louis_aragon]).
linha(8,[balard,lourmel,boucicaut,felix_faure,commerce,la_motte_picquet_grenelle,ecole_militaire,la_tour_maubourg,invalides,concorde,madeleine,opera,richelieu_drouot,grands_boulevards,bonne_nouvelle,strasbourg_saint_denis,republique,filles_du_calvaire,saint_sebastien_froissart,chemin_vert,bastille,ledru_rollin,faidherbe_chaligny,reuilly_diderot,montgallet,daumesnil,michel_bizot,porte_doree,porte_de_charenton,liberte,charenton_ecoles,ecole_veterinaire_de_maisons_alfort,maisons_alfort_stade,maisons_alfort_les_juilliottes,creteil_l_echat,creteil_universite,creteil_prefecture,pointe_du_lac]).
linha(9,[pont_de_sevres,billancourt,marcel_sembat,porte_de_saint_cloud,exelmans,michel_ange_molitor,michel_ange_auteuil,jasmin,ranelagh,la_muette,rue_de_la_pompe,trocadero,iena,alma_marceau,franklin_d_roosevelt,saint_philippe_du_roule,miromesnil,saint_augustin,havre_caumartin,chaussee_d_antin_la_fayette,richelieu_drouot,grands_boulevards,bonne_nouvelle,strasbourg_saint_denis,republique,oberkampf,saint_ambroise,voltaire,charonne,rue_des_boulets,nation,buzenval,maraichers,porte_de_montreuil,robespierre,croix_de_chavaux,mairie_de_montreuil]).
/* linha 10 < sentido 1 > Gare d'Austerlitz */
linha(10,[boulogne_pont_de_saint_cloud,boulogne_jean_jaures,michel_ange_molitor,chardon_lagache,mirabeau,javel_andre_citroen,charles_michels,avenue_emile_zola,la_motte_picquet_grenelle,segur,duroc,vaneau,sevres_babylone,mabillon,odeon,cluny_la_sorbonne,maubert_mutualit�,cardinal_lemoine,jussieu,gare_d_austerlitz]).
/* linha 10 < sentido 2 > Boulogne Pont de ST-cloud */
linha(10,[gare_d_austerlitz,jussieu,cardinal_lemoine,maubert_mutualit�,cluny_la_sorbonne,odeon,mabillon,sevres_babylone,vaneau,duroc,segur,la_motte_picquet_grenelle,avenue_emile_zola,charles_michels,javel_andre_citroen,eglise_d_auteuil,michel_ange_auteuil,porte_d_auteuil,boulogne_jean_jaures,boulogne_pont_de_saint_cloud]).
linha(11,[chatelet,hotel_de_ville,rambuteau,arts_et_metiers,republique,goncourt,belleville,pyrenees,jourdain,place_des_fetes,telegraphe,porte_des_lilas,mairie_des_lilas]).
linha(12,[front_populaire,porte_de_la_chapelle,marx_dormoy,marcadet_poissonniers,jules_joffrin,lamarck_caulaincourt,abesses,pigalle,saint_georges,notre_dame_de_lorette,trinite_d_estienne_d_orves,saint_lazare,madeleine,concorde,assemblee_nationale,solferino,rue_du_bac,sevres_babylone,rennes,notre_dame_des_champs,montparnasse_bienvenue,falguiere,pasteur,volontaires,vaugirard,convention,porte_de_versailles,corentin_celton,mairie_d_issy]).
/* Linha 13 < sentido 1 > Saint-Denis-Universit� */
linha(13,[saint_denis_universite,basilique_de_saint_denis,saint_denis_porte_de_paris,carrefour_pleyel,mairie_de_saint_ouen,garibaldi,porte_de_saint,guy_moquet,la_fourche,place_de_clichy,liege,saint_lazare,miromesnil,champs_elysees_clemenceau,invalides,varenne,saint_fran�ois_xavier,duroc,montparnasse_bienvenue,gaite,pernety,plaisance,porte_de_vanves,malakoff_plateau_de_vanves,malakoff_rue_etienne_Dolet,chatillon_montrouge]).
/* Linha 13 < sentido 2 > Asnieres-Gennevilliers Les Courtilles */
linha(13,[asnieres_gennevilliers_les_courtilles,les_agnettes,gabriel_peri,mairie_de_clichy,porte_de_clichy,brochant,la_fourche,place_de_clichy,liege,saint_lazare,miromesnil,champs_elysees_clemenceau,invalides,varenne,saint_fran�ois_xavier,duroc,montparnasse_bienvenue,gaite,pernety,plaisance,porte_de_vanves,malakoff_plateau_de_vanves,malakoff_rue_etienne_Dolet,chatillon_montrouge]).
linha(14,[saint_lazare,madeleine,pyramides,chatelet,gare_de_lyon,bercy,cour_saint_emilion,bibliotheque_fran�ois_mitterrand,olympiades]).
/* linha 3bis */
linha(30,[porte_des_lilas,saint_fargeau,pelleport,gambetta]).
/* linha 7 bis < sentido 1 > Pre St-Gervals */
linha(70,[louis_blanc,jaures,bolivar,buttes_chaumont,botzaris,place_des_fetes,pre_saint_gervais]).
/* linha 7 bis < sentido 2> Louis Blanc */
linha(70,[pre_saint_gervais,danube,botzaris,buttes_chaumont,bolivar,jaures,louis_blanc]).


/* Liga��es das linhas */
% linha(LINHA1,LINHA2,TEMPO). << TEMPO - tempo viagem da liga��o linha1 at� linha2 >>

/* LINHA 1 */
liga(la_defense,esplanade_de_la_defense,2).
liga(esplanade_de_la_defense,pont_de_neuilly,2).
liga(pont_de_neuilly,les_sablons,2).
liga(les_sablons,porte_maillot,2).
liga(porte_maillot,argentine,2).
liga(argentine,charles_de_gaulle_etoile,2).
liga(charles_de_gaulle_etoile,george_v,2).
liga(george_v,franklin_d_roosevelt,2).
liga(franklin_d_roosevelt,champs_elysees_clemenceau,2).
liga(champs_elysees_clemenceau,concorde,2).
liga(concorde,tuileries,2).
liga(tuileries,palais_royal_musee_du_louvre,2).
liga(palais_royal_musee_du_louvre,louvre_rivoli,2).
liga(louvre_rivoli,chatelet,2).
liga(chatelet,hotel_de_ville,2).
liga(hotel_de_ville,saint_paul,2).
liga(saint_paul,bastille,2).
liga(bastille,gare_de_lyon,2).
liga(gare_de_lyon,reuilly_diderot,2).
liga(reuilly_diderot,nation,2).
liga(nation,porte_de_vincennes,2).
liga(porte_de_vincennes,saint_mande,2).
liga(saint_mande,berault,2).
liga(berault,chateau_de_vincennes,2).

/* LINHA 2 */
liga(porte_dauphine,victor_hugo,2).
liga(victor_hugo,charles_de_gaulle_etoile,2).
liga(charles_de_gaulle_etoile,ternes,2).
liga(ternes,courcelles,2).
liga(courcelles,monceau,2).
liga(monceau,villiers,2).
liga(villiers,rome,2).
liga(rome,place_de_clichy,2).
liga(place_de_clichy,blanche,2).
liga(blanche,pigalle,2).
liga(pigalle,anvers,2).
liga(anvers,barbes_rochechouart,2).
liga(barbes_rochechouart,la_chapelle,2).
liga(la_chapelle,stalingrad,2).
liga(stalingrad,jaures,2).
liga(jaures,colonel_fabien,2).
liga(colonel_fabien,belleville,2).
liga(belleville,couronnes,2).
liga(couronnes,menilmontant,2).
liga(menilmontant,pere_lachaise,2).
liga(pere_lachaise,philippe_auguste,2).
liga(philippe_auguste,alexandre_dumas,2).
liga(alexandre_dumas,avron,2).
liga(avron,nation,2).

/* LINHA 3 */
liga(pont_de_levallois_becon,anatole_france,2).
liga(anatole_france,louise_michel,2).
liga(louise_michel,porte_de_champerret,2).
liga(porte_de_champerret,pereire,2).
liga(pereire,wagram,2).
liga(wagram,malesherbes,2).
liga(malesherbes,villiers,2).
liga(villiers,europe,2).
liga(europe,saint_lazare,2).
liga(saint_lazare,havre_caumartin,2).
liga(havre_caumartin,opera,2).
liga(opera,quatre_septembre,2).
liga(quatre_septembre,bourse,2).
liga(bourse,sentier,2).
liga(sentier,reaumur_sebastopol,2).
liga(reaumur_sebastopol,arts_et_metiers,2).
liga(arts_et_metiers,temple,2).
liga(temple,republique,2).
liga(republique,parmentier,2).
liga(parmentier,rue_saint_maur,2).
liga(rue_saint_maur,pere_lachaise,2).
liga(pere_lachaise,gambetta,2).
liga(gambetta,porte_de_bagnolet,2).
liga(porte_de_bagnolet,gallieni,2).

/* LINHA 4 */
liga(porte_de_clignancourt,simplon,2).
liga(simplon,marcadet_poissonniers,2).
liga(marcadet_poissonniers,chateau_rouge,2).
liga(chateau_rouge,barbes_rochechouart,2).
liga(barbes_rochechouart,gare_du_nord,2).
liga(gare_du_nord,gare_de_lest,2).
liga(gare_de_lest,chateau_deau,2).
liga(chateau_deau,strasbourg_saint_denis,2).
liga(strasbourg_saint_denis,reaumur_sebastopol,2).
liga(reaumur_sebastopol,etienne_marcel,2).
liga(etienne_marcel,les_halles,2).
liga(les_halles,chatelet,2).
liga(chatelet,cite,2).
liga(cite,saint_michel,2).
liga(saint_michel,odeon,2).
liga(odeon,saint_germain_des_pres,2).
liga(saint_germain_des_pres,saint_sulpice,2).
liga(saint_sulpice,saint_placide,2).
liga(saint_placide,montparnasse_bienvenue,2).
liga(montparnasse_bienvenue,vavin,2).
liga(vavin,raspail,2).
liga(raspail,denfert_rochereau,2).
liga(denfert_rochereau,mouton_duvernet,2).
liga(mouton_duvernet,alesia,2).
liga(alesia,porte_dorleans,2).
liga(porte_dorleans,mairie_de_montrouge,2).

/* LINHA 5 */
liga(bobigny_pablo_picasso,bobigny_pantin_raymon_queneau,2).
liga(bobigny_pantin_raymon_queneau,eglise_de_pantin,2).
liga(eglise_de_pantin,hoche,2).
liga(hoche,porte_de_pantin,2).
liga(porte_de_pantin,ourcq,2).
liga(ourcq,laumiere,2).
liga(laumiere,jaures,2).
liga(jaures,stalingrad,2).
liga(stalingrad,gare_du_nord,2).
liga(gare_du_nord,gare_de_lest,2).
liga(gare_de_lest,jacques_bonsergent,2).
liga(jacques_bonsergent,republique,2).
liga(republique,oberkampf,2).
liga(oberkampf,richard_lenoir,2).
liga(richard_lenoir,breguet_sabin,2).
liga(breguet_sabin,bastille,2).
liga(bastille,quai_de_la_rapee,2).
liga(quai_de_la_rapee,gare_dausterlitz,2).
liga(gare_dausterlitz,saint_marcel,2).
liga(saint_marcel,campo_formio,2).
liga(campo_formio,place_ditalie,2).
/*
/* LINHA 6 */
liga(charles_de_gaulle_etoile,kleber,2).
liga(kleber,boissiere,2).
liga(boissiere, trocadero,2).
liga(trocadero,passy,2).
liga(passy,bir_hakeim,2).
liga(bir_hakeim,dupleix,2).
liga(dupleix,la_motte_picquet_grenelle,2).
liga(la_motte_picquet_grenelle,cambronne,2).
liga(cambronne,sevres_lecourbe,2).
liga(sevres_lecourbe,pasteur,2).
liga(pasteur,montparnasse_bienvenue,2).
liga(montparnasse_bienvenue,edgar_quinet,2).
liga(edgar_quinet,raspail,2).
liga(raspail,denfert_rochereau,2).
liga(denfert_rochereau,saint_jacques,2).
liga(saint_jacques,glaciere,2).
liga(glaciere,corvisart,2).
liga(corvisart,place_ditalie,2).
liga(place_ditalie,nationale,2).
liga(nationale,chevaleret,2).
liga(chevaleret,quai_de_la_gare,2).
liga(quai_de_la_gare,bercy,2).
liga(bercy,dugommier,2).
liga(dugommier,daumesnil,2).
liga(daumesnil,bel_air,2).
liga(bel_air,picpus,2).
liga(picpus,nation,2).

/* LINHA 7 */
liga(la_courneuve_8_Mai_1945,fort_d_aubervilliers,2).
liga(fort_d_aubervilliers,aubervilliers_pantin_quatre_chemins,2).
liga(aubervilliers_pantin_quatre_chemins,porte_de_la_villette,2).
liga(porte_de_la_villette,corentin_cariou,2).
liga(corentin_cariou,crimee,2).
liga(crimee,riquet,2).
liga(riquet,stalingrad,2).
liga(stalingrad,louis_blanc,2).
liga(louis_blanc,chateau_landon,2).
liga(chateau_landon,gare_de_l_est,2).
liga(gare_de_l_est,poissonniere,2).
liga(poissonniere,cadet,2).
liga(cadet,le_peletier,2).
liga(le_peletier,chaussee_d_antin_la_fayette,2).
liga(chaussee_d_antin_la_fayette,opera,2).
liga(opera,pyramides,2).
liga(pyramides,palais_royal_musee_du_louvre,2).
liga(palais_royal_musee_du_louvre,pont_neuf,2).
liga(pont_neuf,chatelet,2).
liga(chatelet,pont_marie,2).
liga(pont_marie,sully_morland,2).
liga(sully_morland,jussieu,2).
liga(jussieu,place_monge,2).
liga(place_monge,censier_daubenton,2).
liga(censier_daubenton,les_gobelins,2).
liga(les_gobelins,place_d_italie,2).
liga(place_d_italie,tolbiac,2).
liga(tolbiac,maison_blanche,2).
/* Linha 7 < sentido 1 > MARIE_D'IVRY */
liga(maison_blanche,porte_d_italie,2).
liga(porte_d_italie,porte_de_choisy,2).
liga(porte_de_choisy,porte_d_ivry,2).
liga(porte_d_ivry,pierre_et_marie_curie,2).
liga(pierre_et_marie_curie,mairie_d_ivry,2).
/* linha 7 < sentido 2 > Villejuif_Louis Aragon */
liga(maison_blanche,le_kremlin_bicetre,2).
liga(le_kremlin_bicetre,villejuif_leo_lagrange,2).
liga(villejuif_leo_lagrange,villejuif_paul_vaillant_couturier,2).
liga(villejuif_paul_vaillant_couturier,villejuif_louis_aragon,2).

/* LINHA 8 */
liga(balard,lourmel,2).
liga(lourmel,boucicaut,2).
liga(boucicaut,felix_faure,2).
liga(felix_faure,commerce,2).
liga(commerce,la_motte_picquet_grenelle,2).
liga(la_motte_picquet_grenelle,ecole_militaire,2).
liga(ecole_militaire,la_tour_maubourg,2).
liga(la_tour_maubourg,invalides,2).
liga(invalides,concorde,2).
liga(concorde,madeleine,2).
liga(madeleine,opera,2).
liga(opera,richelieu_drouot,2).
liga(richelieu_drouot,grands_boulevards,2).
liga(grands_boulevards,bonne_nouvelle,2).
liga(bonne_nouvelle,strasbourg_saint_denis,2).
liga(strasbourg_saint_denis,republique,2).
liga(republique,filles_du_calvaire,2).
liga(filles_du_calvaire,saint_sebastien_froissart,2).
liga(saint_sebastien_froissart,chemin_vert,2).
liga(chemin_vert,bastille,2).
liga(bastille,ledru_rollin,2).
liga(ledru_rollin,faidherbe_chaligny,2).
liga(faidherbe_chaligny,reuilly_diderot,2).
liga(reuilly_diderot,montgallet,2).
liga(montgallet,daumesnil,2).
liga(daumesnil,michel_bizot,2).
liga(michel_bizot,porte_doree,2).
liga(porte_doree,porte_de_charenton,2).
liga(porte_de_charenton,liberte,2).
liga(liberte,charenton_ecoles,2).
liga(charenton_ecoles,ecole_veterinaire_de_maisons_alfort,2).
liga(ecole_veterinaire_de_maisons_alfort,maisons_alfort_stade,2).
liga(maisons_alfort_stade,maisons_alfort_les_juilliottes,2).
liga(maisons_alfort_les_juilliottes,creteil_l_echat,2).
liga(creteil_l_echat,creteil_universite,2).
liga(creteil_universite,creteil_prefecture,2).
liga(creteil_prefecture,pointe_du_lac,2).

/* LINHA 9 */
liga(pont_de_sevres,billancourt,2).
liga(billancourt,marcel_sembat,2).
liga(marcel_sembat,porte_de_saint_cloud,2).
liga(porte_de_saint_cloud,exelmans,2).
liga(exelmans,michel_ange_molitor,2).
liga(michel_ange_molitor,michel_ange_auteuil,2).
liga(michel_ange_auteuil,jasmin,2).
liga(jasmin,ranelagh,2).
liga(ranelagh,la_muette,2).
liga(la_muette,rue_de_la_pompe,2).
liga(rue_de_la_pompe,trocadero,2).
liga(trocadero,iena,2).
liga(iena,alma_marceau,2).
liga(alma_marceau,franklin_d_roosevelt,2).
liga(franklin_d_roosevelt,saint_philippe_du_roule,2).
liga(saint_philippe_du_roule,miromesnil,2).
liga(miromesnil,saint_augustin,2).
liga(saint_augustin,havre_caumartin,2).
liga(havre_caumartin,chaussee_d_antin_la_fayette,2).
liga(chaussee_d_antin_la_fayette,richelieu_drouot,2).
liga(richelieu_drouot,grands_boulevards,2).
liga(grands_boulevards,bonne_nouvelle,2).
liga(bonne_nouvelle,strasbourg_saint_denis,2).
liga(strasbourg_saint_denis,republique,2).
liga(republique,oberkampf,2).
liga(oberkampf,saint_ambroise,2).
liga(saint_ambroise,voltaire,2).
liga(voltaire,charonne,2).
liga(charonne,rue_des_boulets,2).
liga(rue_des_boulets,nation,2).
liga(nation,buzenval,2).
liga(buzenval,maraichers,2).
liga(maraichers,porte_de_montreuil,2).
liga(porte_de_montreuil,robespierre,2).
liga(robespierre,croix_de_chavaux,2).
liga(croix_de_chavaux,mairie_de_montreuil,2).

/* LINHA 10 */
liga(boulogne_pont_de_saint_cloud,boulogne_jean_jaures,2).
/* linha 10 < sentido 1 > Gare d'Austerlitz */
liga(boulogne_jean_jaures,michel_ange_molitor,2).
liga(michel_ange_molitor,chardon_lagache,2).
liga(chardon_lagache,mirabeau,2).
liga(mirabeau,javel_andre_citroen,2).
liga(javel_andre_citroen,charles_michels,2).
liga(charles_michels,avenue_emile_zola,2).
liga(avenue_emile_zola,la_motte_picquet_grenelle,2).
liga(la_motte_picquet_grenelle,segur,2).
liga(segur,duroc,2).
liga(duroc,vaneau,2).
liga(vaneau,sevres_babylone,2).
liga(sevres_babylone,mabillon,2).
liga(mabillon,odeon,2).
liga(odeon,cluny_la_sorbonne,2).
liga(cluny_la_sorbonne,maubert_mutualit�,2).
liga(maubert_mutualit�,cardinal_lemoine,2).
liga(cardinal_lemoine,jussieu,2).
liga(jussieu,gare_d_austerlitz,2).
/* linha 10 < sentido 2 > Boulogne Pont de ST-cloud */
liga(javel_andre_citroen,eglise_d_auteuil,2).
liga(eglise_d_auteuil,michel_ange_auteuil,2).
liga(michel_ange_auteuil,porte_d_auteuil,2).
liga(porte_d_auteuil,boulogne_jean_jaures,2).

/* LINHA 11 */
liga(chatelet,hotel_de_ville,2).
liga(hotel_de_ville,rambuteau,2).
liga(rambuteau,arts_et_metiers,2).
liga(arts_et_metiers,republique,2).
liga(republique,goncourt,2).
liga(goncourt,belleville,2).
liga(belleville,pyrenees,2).
liga(pyrenees,jourdain,2).
liga(jourdain,place_des_fetes,2).
liga(place_des_fetes,telegraphe,2).
liga(telegraphe,porte_des_lilas,2).
liga(porte_des_lilas,mairie_des_lilas,2).

/* LINHA 12 */
liga(front_populaire,porte_de_la_chapelle,2).
liga(porte_de_la_chapelle,marx_dormoy,2).
liga(marx_dormoy,marcadet_poissonniers,2).
liga(marcadet_poissonniers,jules_joffrin,2).
liga(jules_joffrin,lamarck_caulaincourt,2).
liga(lamarck_caulaincourt,abesses,2).
liga(abesses,pigalle,2).
liga(pigalle,saint_georges,2).
liga(saint_georges,notre_dame_de_lorette,2).
liga(notre_dame_de_lorette,trinite_d_estienne_d_orves,2).
liga(trinite_d_estienne_d_orves,saint_lazare,2).
liga(saint_lazare,madeleine,2).
liga(madeleine,concorde,2).
liga(concorde,assemblee_nationale,2).
liga(assemblee_nationale,solferino,2).
liga(solferino,rue_du_bac,2).
liga(rue_du_bac,sevres_babylone,2).
liga(sevres_babylone,rennes,2).
liga(rennes,notre_dame_des_champs,2).
liga(notre_dame_des_champs,montparnasse_bienvenue,2).
liga(montparnasse_bienvenue,falguiere,2).
liga(falguiere,pasteur,2).
liga(pasteur,volontaires,2).
liga(volontaires,vaugirard,2).
liga(vaugirard,porte_de_versailles,2).
liga(porte_de_versailles,corentin_celton,2).
liga(corentin_celton,mairie_d_issy,2).

/* LINHA 13 */
/* Linha 13 < sentido 1 > Saint-Denis-Universit� */
liga(saint_denis_universite,basilique_de_saint_denis,2).
liga(basilique_de_saint_denis,saint_denis_porte_de_paris,2).
liga(saint_denis_porte_de_paris,carrefour_pleyel,2).
liga(carrefour_pleyel,mairie_de_saint_ouen,2).
liga(mairie_de_saint_ouen,garibaldi,2).
liga(garibaldi,porte_de_saint,2).
liga(porte_de_saint,guy_moquet,2).
liga(guy_moquet,la_fourche,2).
/* Linha 13 < sentido 2 > Asnieres-Gennevilliers Les Courtilles */
liga(asnieres_gennevilliers_les_courtilles,les_agnettes,2).
liga(les_agnettes,gabriel_peri,2).
liga(gabriel_peri,mairie_de_clichy,2).
liga(mairie_de_clichy,porte_de_clichy,2).
liga(porte_de_clichy,brochant,2).
liga(brochant,la_fourche,2).
/* Junta duas linhas na estacao La Fourche */
liga(la_fourche,place_de_clichy,2).
liga(place_de_clichy,liege,2).
liga(liege,saint_lazare,2).
liga(saint_lazare,miromesnil,2).
liga(miromesnil,champs_elysees_clemenceau,2).
liga(champs_elysees_clemenceau,invalides,2).
liga(invalides,varenne,2).
liga(varenne,saint_fran�ois_xavier,2).
liga(saint_fran�ois_xavier,duroc,2).
liga(duroc,montparnasse_bienvenue,2).
liga(montparnasse_bienvenue,gaite,2).
liga(gaite,pernety,2).
liga(pernety,plaisance,2).
liga(plaisance,porte_de_vanves,2).
liga(porte_de_vanves,malakoff_plateau_de_vanves,2).
liga(malakoff_plateau_de_vanves,malakoff_rue_etienne_Dolet,2).
liga(malakoff_rue_etienne_Dolet,chatillon_montrouge,2).

/* LINHA 14 */
liga(saint_lazare,madeleine,2).
liga(madeleine,pyramides,2).
liga(pyramides,chatelet,2).
liga(chatelet,gare_de_lyon,2).
liga(gare_de_lyon,bercy,2).
liga(bercy,cour_saint_emilion,2).
liga(cour_saint_emilion,bibliotheque_fran�ois_mitterrand,2).
liga(bibliotheque_fran�ois_mitterrand,olympiades,2).

/* LINHA 7bis */
liga(louis_blanc,jaures,2).
liga(jaures,bolivar,2).
liga(bolivar,buttes_chaumont,2).
liga(buttes_chaumont,botzaris,2).
/* linha 7 bis < sentido 1 > Pre St-Gervals */
liga(botzaris,place_des_fetes,2).
liga(place_des_fetes,pre_saint_gervais,2).
/* linha 7 bis < sentido 2> Louis Blanc */
liga(pre_saint_gervais,danube,2).
liga(danube,botzaris,2).

/* LINHA 3bis */
liga(porte_des_lilas,saint_fargeau,2).
liga(saint_fargeau,pelleport,2).
liga(pelleport,gambetta,2).
*/


%%turismo(local,lista de dias(1=domingo,2=segunda,3=ter�a, 4=....7=sabado),hora abertura,hora fecho,tempo visita em horass,esta�ao)
turismo('Bowling de Paris La Chapelle',[1,2,3,4,5],10,2,2,'porte de la chapelle').
turismo('Bowling de Paris La Chapelle',[6,7],0,4,2,'porte de la chapelle').
turismo('Eglise Saint-Denys de la Chapelle',[2,4,5,6,7],8,19,2,'marx dormoy').
turismo('Basilique du Sacre Coeur',[1,2,3,4,5,6,7],7,4,1,'abbesses').
turismo('Musee Gustave Moreau',[2,3,5],10,13,1,'saint georges').
turismo('Musee Gustave Moreau',[2,3,5],14,17,1,'saint georges').
turismo('Musee Gustave Moreau',[6,7,8],10,17,1,'saint georges').
turismo('Musee de la Poste',[2,3,4,5,6],10,18,1,'montparnasse bienvenue').
turismo('Institut et Musee Pasteur',[2,3,4,5,6],14,16,1,'pasteur').
turismo('Mairie Issy les Moulineaux',[2,3,4,5,6],9,18,1,'mairie d issy').
turismo('FNAC Saint Lazare',[2,3,4,5,6,7],10,20,1,'saint lazare').
turismo('Tour Eiffel',[1,2,3,4,5,6,7],9,11,1,'ecole militaire').
turismo('Palais du Louvre',[1,2,5,7],9,22,1,'louvre rivoli').
turismo('Notre Dame',[2,3,4,5,6],10,18,1,'cite').
turismo('Arc de Triomphe',[1,2,3,4,5,6,7],10,22,1,'charles de gaulle etoille').

/*  Pontos de interesse tur�sticos  */
% exemplo: ponto_de_interesse(interesse,esta�ao_proxima,tempo_visita,[linhas],[[dias_abertos],horainicio,horafim]).

ponto_de_interesse(sacre_coeur_mont_martre,anvers,30,[2],[1,2,3,4,5,6,7],[[6,0,22,0],[6,0,22,0],[6,0,22,0]]).
ponto_de_interesse(city_hall_of_paris,hotel_de_ville,70,[1,11],[1,2,3,4,5],[[0,0,24,0],[],[]]).
ponto_de_interesse(eiffel_tower,bir_hakeim,70,[6],[1,2,3,4,5,6,7],[[9,0,23,0],[9,0,23,0],[9,0,23,0]]).
ponto_de_interesse(arc_de_triomphe,charles_de_gaulle_etoile,40,[1,2,6],[1,2,3,4,5,6,7],[[10,0,22,0],[10,0,22,0],[10,0,22,0]]).
ponto_de_interesse(royal_palace_and_louvre,palais_royal_musee_du_louvre,30,[1,7],[1,3,4,5,6,7],[[9,0,18,0],[9,0,18,0],[9,0,18,0]]).
ponto_de_interesse(notre_dame,saint_michel,60,[4],[1,2,3,4,5,6,7],[[8,0,19,0],[8,0,19,0],[8,0,19,0]]).
ponto_de_interesse(boulevard_Haussmann,havre_caumartin,40,[3,9],[1,2,3,4,5,6,7],[[0,0,24,0],[0,0,24,0],[0,0,24,0]]).
ponto_de_interesse(champs_elysees,champs_elysees_clemenceau,30,[1,13],[1,2,3,4,5,6],[[10,0,24,0],[10,0,24,0],[12,0,23,0]]).
