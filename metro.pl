:- include('linhas.pl').

%Mostra todos os resultados de uma lista
%set_prolog_flag(toplevel_print_options,[quoted(true), portray(true)]).

:-dynamic cruza/3,estacao_linhas/2,estacoes/1.

inicializa:-gera_estacoes,gera_cruzamentos,gera_estacoes_linhas.

%% GERA ESTACOES %%
gera_estacoes:-	findall(L,linha(_,L),LE),
		une(LE,LEstacoes),
		asserta(estacoes(LEstacoes)).

une([LH|LT],LU):-une(LT,LU1),uniao(LH,LU1,LU).
une([],[]).

uniao([],L,L).
uniao([X|L],L1,L2):-member(X,L1),!,uniao(L,L1,L2).
uniao([X|L],L1,[X|L2]):-uniao(L,L1,L2).

%% GERA CRUZAMENTO %%
gera_cruzamentos:-findall(_,cruzamento,_).

cruzamento:-	linha(N1,LE1),
		linha(N2,LE2),
		N1\==N2,
		interseccao(LE1,LE2,LI),
		assertz(cruza(N1,N2,LI)).

interseccao([],_,[]).
interseccao([X|L],L1,[X|L2]):-member(X,L1),!,interseccao(L,L1,L2).
interseccao([_|L],L1,L2):-interseccao(L,L1,L2).

%% GERA ESTACAO LINHAS
%% esta��o_linhas(esta��o,lista_de_todas_linhas_que_passam_pela_esta��o).
%% %%
gera_estacoes_linhas:-findall(_,(estacoes(LE),member(E,LE),todas_linhas(E,LL),assertz(estacao_linhas(E,LL))),_).

todas_linhas(E,LL):-findall(Linha,(linha(Linha,LEL),member(E,LEL)),LL).

%% Gera caminho (estacao1,estacao2,linha)
%
gera_caminho(E1,E2,LC):-estacao_linhas(E1,LE1),estacao_linhas(E2,LE2),caminho(E1,LE1,E2,LE2,[],LC).


caminho(E1,LE1,E2,LE2,_,[(E1,E2,Linha)]):-interseccao(LE1,LE2,[H|T]),member(Linha,[H|T]),!.
caminho(E1,LE1,E2,LE2,LLV,[(E1,EI,Linha)|LC]):-member(Linha,LE1),
					(not(member(Linha,LLV))),
					cruza(Linha,_,L),
					member(EI,L),
					estacao_linhas(EI,LEI),
					caminho(EI,LEI,E2,LE2,[Linha|LLV],LC).

%% Ponto 3
% a) Menor troca de linha (estacao1,estacao2,linha)
%

menorTrocaLinha(E1,E2,LMTL):-findall(L,gera_caminho(E1,E2,L),LL),menor(LL,LMTL).

menor([H],H).
menor([H|T],H):-menor(T,L1),length(H,C),length(L1,C1),C<C1,!.
menor([_|T],L1):-menor(T,L1).


%M�todo de pesquisa da linha mais rapida atraves do atributo distancia
% b) maisRapido(Origem,Destino,Percurso,Distancia)
%
maisRapido(Origem,Destino,Perc,Dist):-
	maisRapido1([(0,[Origem])],Destino,P,Dist),
	reverse(P,Perc).

maisRapido1([(Dist,[D|T])|_],D,[D|T],Dist).
maisRapido1([(C,[A|T])|R],D,P,Dist):-
	proximos(A,C,T,L),
	append(R,L,N),
	sort(N,NL),
	maisRapido1(NL,D,P,Dist).

proximos(A,C,T,L):- findall((Cs,[S,A|T]), ((liga(A,S,Ca); liga(S,A,Ca)),Cs is C + Ca), L).



%%	%%%%%%%%%%%%%%%%%%%%%%%
%
%
%
%
%5-
%planear uma visita
